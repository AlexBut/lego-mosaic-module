from skimage.io import imread
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from collections import Counter
from PIL import Image
import math
import pip

def install():
    if hasattr(pip, 'main'):
        pip.main(['install', 'numpy'])
        pip.main(['install', 'pandas'])
        pip.main(['install', 'matplotlib'])
        pip.main(['install', 'scikit-image'])
        pip.main(['install', 'Pillow'])
    else:
        pip._internal.main(['install', 'numpy'])
        pip._internal.main(['install', 'pandas'])
        pip._internal.main(['install', 'matplotlib'])
        pip._internal.main(['install', 'scikit-image'])
        pip._internal.main(['install', 'Pillow'])

def hex_to_rgb(hex_code, as_float = False):
  '''Function to convert hexadecimal to int array. 
      If as_float=True, will return float32 list with normalized values.'''

  if as_float:
    return tuple(int(hex_code[i:i+2], 16)/255 for i in (0,2,4))
  else:
    return tuple(int(hex_code[i:i+2], 16) for i in (0,2,4))

def distance(vec1, vec2):
  '''Auxiliary function to calc vector len
    vec1 and vec2 are RGB arrays'''

  if len(vec1) != len(vec2) != 3:
    raise Exception('vec1 and vec2 are not a RGB indeces')
  return math.sqrt((vec1[0] - vec2[0])**2 + (vec1[1] - vec2[1])**2 + (vec1[2] - vec2[2])**2)

def get_nearest_color(color_dict, given_color):
  '''Finding the nearest color (i.e. RGB array) from color_dict
      compared to given_color
      
      color_dict: dictionary of color name <str> : color array <int>
      given_color: color array <int>'''

  closest_colors = sorted(color_dict.items(), key=lambda color: distance(color[1], given_color))
  return closest_colors[0]

# let values between (0,0,0) and (150,150,150) had light-grey outline,
# and values from (151,151,151) to (255,255,255) had medium-grey outline
# define bool func to calculate that
def get_outline_color(rgb_color_array):
  mask = [False if el <= 150 else True for el in rgb_color_array]
  mask_sum = sum(mask)

  if mask_sum != 3:
    return [180,180,180]
  else:
    return [150,150,150]

# function to draw outline on the left and bottom edge of square plate
# requared arg - new_image_resolution, i.e. side of square plate
# if set to 1 - no outline
# if set from 2 to 6 - dot in left-bottom corner
# if set to 7 or greater - outline on the edge.
# return - square of side size of new_image_resolution with outline

def draw_outline(new_image_resolution, filling_color):
  if len(filling_color) != 3:
    raise Exception(f'not a RGB code (should be len 3 instead of {len(filling_color)}')

  fill_image = np.full((new_image_resolution, new_image_resolution, 3), filling_color, dtype=np.uint8)

  if new_image_resolution == 1:
    return fill_image
  if new_image_resolution <= 6:
    fill_image[new_image_resolution-1,0] = get_outline_color(filling_color)
    return fill_image
  if new_image_resolution > 6:
    fill_image[0:new_image_resolution, 0] = get_outline_color(filling_color)
    fill_image[new_image_resolution-1, 1:new_image_resolution] = get_outline_color(filling_color)
    return fill_image

def check_offset(actual_h, actual_w, mosaic_h, mosaic_w, offset):
  '''actual_h: height of original image in pixels
    actual_w: width of original image in pixels
    mosaic_h: pixels height of mosaic window, depending on square_side parameter
      Let image height is 8473, then actual_h is also equal 8473.
      If square_side set to 100, then mosaic window will be size of int(8473 / 100) * 100 = 8400,
    mosaic_w: pisels width of mosaic window, depending on square_side parameter
    offset: [x,y] to achieve proper fitting of image

    returns
    offset: referring to the above text of mosaic_h, with such parameters we lost 73 pixels of original 
      image. If x set from 0 to 73, we can try to fit original image much better. If user-defined parameters [x, y]
      greater than that submission, we get 0 value on out-of-the-way coordinate.
      '''
  if len(offset) != 2:
    raise Exception(f'offline shoud be an array of len 2, representing [x,y]')
  check = [0, 0]

  if actual_h - mosaic_h > 0 and actual_h - mosaic_h >= offset[1]:
      check[1] = offset[1]
  if actual_w - mosaic_w > 0 and actual_w - mosaic_w >= offset[0]:
      check[0] = offset[0]

  return check


def get_mosaic_image(image, square_side, color_dict, new_image_resolution=10, offset=[0, 0]):
  '''Function to transform input image to mosaic representation
  image: array of shape(height, width, 3)
  square_side: <int> side size of square sector, which represent convolved and color-averaged pixels of image.
  Should vary for best convolving
  color_dict: see get_nearest_color for more info
  new_image_resolution: parameter to define 'pixel' size of mosaic image
  offset: int array of size 2, representing x, y coordinates.
    Used to set offset for proper image fitting, if mosaic part do not split original image entirely.
    For example, if square_side set to 100 and image width is 8473, then this image will be splitted on 
    84 sectors (int(8473 / square_side) = 84), and 73 pixels will not be used.
    By setting offset[0] you can achieve proper image fitting, if needed. 
    Also see check_offset function for more info.  
  
  return
  new_image: array of shape(height, width, 3), containing mosaic image
  color_counter: dictionary of color name <str>: requared number <int>'''

  if offset[0] >= square_side or offset[1] >= square_side:
    raise Exception(f'offline parameters should be less than square_side = {square_side}')

  w = image.shape[1]
  h = image.shape[0]

  if square_side >=w or square_side >=h:
    raise Exception(f'Your image shape is [{h}, {w}]. Enter another square side.')

  squares_per_width = int(w / square_side)  # width and height of image in 
  squares_per_height = int(h / square_side) # squares of square_side side size

  if offset != [0,0]:
    offset = check_offset(h,w,squares_per_height, squares_per_width, offset)

  # indeces to iterate original image to get a slice
  iterate_width_orig = range(square_side + offset[0], squares_per_width*square_side+square_side+offset[0], square_side) 
  iterate_height_orig = range(square_side + offset[1], squares_per_height*square_side+square_side+offset[1], square_side)

  # to count final details list
  color_counter = Counter()

  new_image = np.zeros((squares_per_height*new_image_resolution, squares_per_width*new_image_resolution, 3), dtype=np.uint8)
  prev_h_orig, prev_w_orig = offset[1], offset[0]
  prev_h_new, prev_w_new = 0, 0
  i_new, j_new = new_image_resolution, new_image_resolution
  
  for i in iterate_height_orig:    
    for j in iterate_width_orig:
      av_color = np.mean(np.mean(image[prev_h_orig:i, prev_w_orig:j], axis=0), axis=0)
      nearest_color = get_nearest_color(color_dict, av_color)
      color_counter.update({nearest_color[0]:1})
      
      new_image[prev_h_new:i_new, prev_w_new:j_new] = draw_outline(new_image_resolution, nearest_color[1])
      prev_w_orig = j
      prev_w_new+=new_image_resolution
      j_new+=new_image_resolution
    prev_h_orig = i
    prev_w_orig = 0
    prev_h_new = i_new
    i_new+=new_image_resolution
    prev_w_new = 0
    j_new = new_image_resolution

  if int(w / square_side)*int(h / square_side) != sum(color_counter.values()):
    raise Exception('unequal number of squares and pictures')   


  return new_image, color_counter


if __name__ == '__main__':
    # packages installation 
    # will install numpy, matplotlib, pandas, pil and scikit-image
    # comment if installed manually, or already have them installed
    install()   
    
    try:
        colors = pd.read_csv("colors.csv")
        colors['rgb_array'] = colors.rgb.apply(hex_to_rgb)
        color_dict = dict(zip(colors[colors.is_trans == 'f'].name, colors[colors.is_trans == 'f'].rgb_array))
    except FileNotFoundError:  
        raise FileNotFoundError('''No file named "colors.csv" was found. \n 
        Please, download the dataset at https://www.kaggle.com/rtatman/lego-database, \n
         unzip it, and put colors.csv to the current directory.''')
    
    print('Hello! please, enter name of your image')
    path = input()
    img = imread(path)   
    
    print(f'Your image has following dimensions: { img.shape[0:2] }')
    print('Enter window size to convolve sectors')
    sq_side = int(input())

    #try:
    mosaic, color_list = get_mosaic_image(img, square_side=sq_side, color_dict=color_dict)
    with open('color list.txt', "w") as f:
        f.write('This guide provides you a list of plate colors and their quantity \n\n')
        [f.write(key + " - " + str(value) + '\n') for key, value in color_list.items()]
        f.close()
    plt.axis('off')
    plt.imsave('mosaic ' + path, mosaic)

    print(f'Done! Name of mosaic image is {"mosaic " + path}')
    #except Exception:
        #print('there was an error occured during the running. See the code for more info')
    














